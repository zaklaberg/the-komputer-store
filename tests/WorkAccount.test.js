import WorkAccount from './../js/WorkAccount'

describe('---------- UNIT: Work account tests ----------', () => {
    it('Should pay for a days work', () => {
        const workAcc = WorkAccount();
        workAcc.work();

        expect(workAcc.getCurrentBalance()).toBe(100);
    });

    
});