import BankAccount from './../js/BankAccount'
import WorkAccount from './../js/WorkAccount'

describe('---------- INTEGRATION: Bank and work account interaction ----------', () => {
    it('Transfers the correct amount from work to bank, with no loan present', () => {
        const workAcc = WorkAccount();
        const bankAcc = BankAccount();

        workAcc.work();
        workAcc.work();

        const prevBankBalance = bankAcc.getCurrentBalance();
        const prevWorkBalance = workAcc.getCurrentBalance();

        workAcc.transferToBankBalance(bankAcc);

        expect(workAcc.getCurrentBalance() === 0 && 
                bankAcc.getCurrentBalance() === (prevBankBalance + prevWorkBalance))
        .toBe(true);
    })

    it('Deducts up to 10% of transfer from work to bank to repay loan', () => {
        const workAcc = WorkAccount();
        const bankAcc = BankAccount();

        bankAcc.takeLoan(500, true);
        workAcc.work();
        workAcc.transferToBankBalance(bankAcc);

        expect(bankAcc.getCurrentLoan()).toBe(490);
    });
})