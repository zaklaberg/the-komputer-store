import BankAccount from './../js/BankAccount'

describe('---------- UNIT: Bank account tests ----------', () => {
    it('Should not give a loan of more than twice the current balance', () => {
        const account = BankAccount();
        const currBalance = account.getCurrentBalance();
        
        expect(() => account.takeLoan(currBalance*2 + 1, true))
        .toThrow("You can't borrow more than twice your current balance");
    });
    
    it('Should not give a second loan without owning a laptop', () => {
        const account = BankAccount();
        const currBalance = account.getCurrentBalance();
        account.takeLoan(currBalance - 1, false);
        account.repayLoan(currBalance -1);

        expect(() => account.takeLoan(currBalance - 1, false))
        .toThrow("You must buy a laptop before taking a second loan.");
    });

    it('Should not give a second loan when another loan is not fully repaid', () => {
        const account = BankAccount();
        const currBalance = account.getCurrentBalance();
        account.takeLoan(currBalance - 1, false);
        expect(() => account.takeLoan(currBalance - 1, true))
        .toThrow('Please pay back your current loan before taking a new one.');
    });
    
    it('Should not give negative loans', () => {
        const account = BankAccount();
        expect(() => account.takeLoan(-100, true))
        .toThrow('You cannot borrow a negative amount of money.');
    });
});
