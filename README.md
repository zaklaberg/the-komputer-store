# The Komputer Store

## Setup
- Clone
- npm install
- npm start (Defaults to port 3000. If it picks something else, edit STORE_URL in Store.js to the correct port.)
- run using LiveServer or npx http-server

## Features
In the glorious komputer store, you can:
- Work endlessly, and receive frustratingly little pay.
- Transfer your pay to your bank account.
- Take loans.
- Repay loans.
- View a set of absolutely stunning laptops available to purchase.
- You can also purchase them with your hard-earned money.