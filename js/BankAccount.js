/*
This is a factory function, and returns an object for managing your bank account. 
Allows you to manage a primary bank account and a single loan.
*/
export default function() {
    let currentBalance = 133700;
    let currentLoan = 0;
    let hasLoaned = false;

    const getCurrentBalance = () => currentBalance;
    const addToBalance = amount => currentBalance += amount;
    const removeFromBalance = amount => currentBalance -= amount;

    const getCurrentLoan = () => currentLoan;

    // If eligible, a loan is given for the current amount and added to the balance.
    const takeLoan = (amount, ownsALaptop) => {
        // No loan greater than double the current balance
        if (amount > 2*currentBalance) {
            throw new Error("You can't borrow more than twice your current balance");
        }

        // No more than one loan at a time
        if (currentLoan > 0) {
            throw new Error('Please pay back your current loan before taking a new one.');
        }

        // Must buy at least one computer before taking a second loan
        if (hasLoaned && !ownsALaptop) {
            throw new Error("You must buy a laptop before taking a second loan.")
        }

        // Can't borrow a negative amount
        if (amount < 0) {
            throw new Error('You cannot borrow a negative amount of money.');
        }

        // Everything fine, give the loan.
        currentLoan += amount;
        currentBalance += amount;
        
        if (!hasLoaned) {
            hasLoaned = true;
        }
    }

    // Pays down current loan with amount given.
    // If amount is in excess of current loan, the remainder is returned.
    const repayLoan = amount => {
        const leftOver = currentLoan - amount;

        if (leftOver < 0) {
            currentLoan = 0;
            return -leftOver;
        }
        else {
            currentLoan = leftOver;
            return 0;
        }
    }

    // Public methods for the user to interact with.
    return  {
        getCurrentBalance,
        addToBalance,
        removeFromBalance,
        getCurrentLoan,
        takeLoan,
        repayLoan,
    }
}