/*
A factory, returning an object with which you can manage your job.
Allows you to work, transfer your pay to your bank, and repay your loans.
*/
export default function() {
    let currentBalance = 0;
    const SALARY_PER_DAY = 100;

    const work = () => currentBalance += SALARY_PER_DAY;
    const getCurrentBalance = () => currentBalance;

    // Transfers the current balance to the given bank account. 
    // If a loan is present, the bank will take up to 10% of the amount transferred.
    const transferToBankBalance = bankAccount => {
        // If there are no money to transfer, we won't bother contacting the bank
        if (currentBalance === 0) {
            return;
        }
        // If the owner has a loan at this bank, automatically repay it with up to 10% of current balance of work money.
        if (bankAccount.getCurrentLoan() > 0) {
            const moneyToRepayLoan = Math.floor(currentBalance / 10);
            const remainder = bankAccount.repayLoan(moneyToRepayLoan);
            currentBalance -= (moneyToRepayLoan - remainder);
        }

        bankAccount.addToBalance(currentBalance);
        currentBalance = 0;
    }

    // Attempts to repay a loan with ALL money available.
    // Leftovers will stay in this account and will NOT be transferred to the bank.
    const repayLoan = bankAccount => {
        currentBalance = bankAccount.repayLoan(currentBalance);
    }

    // Public methods for the user to interact with.
    return {
        work,
        transferToBankBalance,
        repayLoan,
        getCurrentBalance
    }
}