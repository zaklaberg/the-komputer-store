import Person from './Person.js'
import Store from './Store.js'
import MoneyPrompt from './MoneyPrompt.js'

class KomputerStore {
    // Used by displayMessage()
    MESSAGE_TYPE = {
        ERROR: 1,
        INFO: 2,
        FRIENDLY: 3
    }

    constructor() {
        this.user = new Person('Zak');
        this.loanMoneyPrompt = new MoneyPrompt('html', 'How much would you like to loan?', 'Loan', this.takeLoan.bind(this));

        // A (frustrated) user may select several laptops in a row. These will then resolve in order. 
        // This isn't a big problem on fast interwebses, but on slow these "extra" resolves are annoying to watch.
        // So, we keep track of the latest request and only render an information update if the id matches.
        // Could alternatively have been solved with a stack of requests.
        this.latestLaptopRequestId = 0;
        this.currentLaptopRequestId = 0;

        // Get interactive elements(buttons, displays)
        const elAllButtons = [...document.querySelectorAll('a.button')];
        const elGetLoanBtn = elAllButtons.filter(btn => btn.text === 'Get a loan')[0]; 
        const elWorkBtn = elAllButtons.filter(btn => btn.text === 'Work')[0]; 
        const elTransferPayToBankBtn = elAllButtons.filter(btn => btn.text === 'Transfer to bank')[0];
        this.elRepayLoanBtn = elAllButtons.filter(btn => btn.text === 'Repay loan')[0];
        this.elLaptopSelector = document.getElementsByTagName('select')[0];
        this.elMessageComponent = document.querySelector('#messageFooter');
        this.elMessageCloseBtn = this.elMessageComponent.querySelector('img');

        this.elLaptopLoadingSpinner = document.querySelector('#laptopContainer > span > span');
        this.elStoreFeatureListDisplay = document.querySelector('.management.store ul');
        this.elBankBalanceDisplay = document.querySelector(".management.bank > .moneyItem:first-of-type > p:last-child");
        this.elBankLoanDisplay = document.querySelector(".management.bank > .moneyItem:last-of-type");
        this.elWorkPayDisplay = document.querySelector(".management.work > .moneyItem > p:last-child");
        this.elLaptopInfoDisplay = document.querySelector('#laptopContainer .laptopInfo');
        this.elOwnedLaptopsDisplay = document.querySelector('.management.inventory ul');

        // Attach event listeners
        elGetLoanBtn.addEventListener('click', this.loanMoneyPrompt.show.bind(this.loanMoneyPrompt));
        elWorkBtn.addEventListener('click', this.work.bind(this));
        elTransferPayToBankBtn.addEventListener('click', this.transferWorkBalanceToBank.bind(this));
        this.elRepayLoanBtn.addEventListener('click', this.repayLoan.bind(this));
        this.elLaptopSelector.addEventListener('change', this.handleSelectedLaptopChanged.bind(this));
        this.elMessageCloseBtn.addEventListener('mouseenter', this.handleMessageCloseButtonEvents.bind(this)); 
        this.elMessageCloseBtn.addEventListener('mouseout', this.handleMessageCloseButtonEvents.bind(this)); 
        this.elMessageCloseBtn.addEventListener('click', this.handleMessageCloseButtonEvents.bind(this)); 

         // Update UI with current information
        this.loadAndDisplayAvailableLaptops();
        this.displayBalances();
        this.displayRepayLoanButton();
    }

    // Displays a message to the user, with background color determined by messageType.
    // If a new message is displayed while an old one is already visible, it will flash 
    // to get the user's attention.
    displayMessage(msg, messageType) { 
        const previousMessageVisible = this.elMessageComponent.style.display !== 'none' ? true : false;
        this.elMessageComponent.children[0].innerHTML = msg;
        this.elMessageComponent.style.display = 'flex';

        let finalVisualClass = 'error';
        if (messageType === this.MESSAGE_TYPE.INFO) {
            finalVisualClass = 'info';
        }
        else if (messageType === this.MESSAGE_TYPE.FRIENDLY) {
            finalVisualClass = 'friendly';
        }

        // If no previous message, just set the final visual class
        if (!previousMessageVisible) {
            this.elMessageComponent.className = finalVisualClass;
            return;
        }

        // If a message is visible, flash the new one
        this.elMessageComponent.className = 'whiteHighlight';
        setTimeout(() => {
            this.elMessageComponent.className = finalVisualClass;
        }, 100)
    }

    handleMessageCloseButtonEvents(e) {
        switch(e.type) {
            case 'mouseenter':
                this.elMessageCloseBtn.src = './images/close-button-highlight.png';
                break;
            case 'mouseout':
                this.elMessageCloseBtn.src = './images/close-button.png'
                break;
            case 'click':
                this.elMessageComponent.style.display = 'none';
                break;
        }
    }

    handleSelectedLaptopChanged(e) {
        // Hide info display while we work(or if check below is met)
        this.hideLaptopInfo();

        // The first element in the laptop <select> is --Select a laptop--
        // If it's chosen, we need do nothing else. (Could omit, but it saves us a needless HTTP req)
        if (e.target.value === this.elLaptopSelector.options[0].value) return;
        
        const laptopId = parseInt(e.target.value, 10);
        this.updateLaptopInfo(laptopId);
        this.showLaptopInfo();
    }

    loadAndDisplayAvailableLaptops() {
        const makeLaptopsSelectableInUI = laptops => {
            laptops.forEach(laptop => {
                const elOpt = document.createElement('option');
                elOpt.text = laptop.name;
                elOpt.value = laptop.id;
                this.elLaptopSelector.appendChild(elOpt);
            });
        };

        // TODO: This belongs in an e2e test. Add cypress.
        const addFakeLaptopChoice = () => {
            const elFakeOpt = document.createElement('option');
            elFakeOpt.text = 'Elusive Hard2Get 5000';
            elFakeOpt.value = 5;
            this.elLaptopSelector.appendChild(elFakeOpt);
        };

        this.showLaptopLoadingSpinner();
        Store.getAvailableLaptops()
            .then(makeLaptopsSelectableInUI)
            .catch(this.displayMessage.bind(this))
            .finally(this.hideLaptopLoadingSpinner.bind(this))
            .finally(addFakeLaptopChoice);
    }


    showLaptopLoadingSpinner() {
        this.elLaptopLoadingSpinner.style.visibility = 'visible';
        this.elLaptopLoadingSpinner.style.opacity = 1;
    }

    /*
        Two bad things happen here. 
        1) Hardcoded timer corresponds to transition timer in css. Should be linked for maintainability
           https://css-tricks.com/getting-javascript-to-talk-to-css-and-sass/
        2) It takes the time of the transition before the underlying elements can be interacted with.
           Presumably not a huge problem, since users may want to read about the laptop before buying.

        Can hide instantly or with a transition(for prettiness).
        Resolves a promise when transition is complete, so we can
        delay rendering of underlying content(which user otherwise could see but not interact with)
    */
    async hideLaptopLoadingSpinner(instantly = false) {
        this.elLaptopLoadingSpinner.style.opacity = 0;
        if (instantly) {
            this.elLaptopLoadingSpinner.style.visibility = 'hidden';
            return;
        }
        
        return new Promise(resolve => {
                setTimeout(() => {
                    this.elLaptopLoadingSpinner.style.visibility = 'hidden';
                    resolve();
            }, 100);
        })
    }

    showLaptopInfo() {
        this.elLaptopInfoDisplay.style.visibility = 'visible';
        this.elStoreFeatureListDisplay.style.visibility = 'visible';
    }

    hideLaptopInfo() {
        this.elLaptopInfoDisplay.style.visibility = 'hidden';
        this.elStoreFeatureListDisplay.style.visibility = 'hidden';
    }

    // Attempts to get laptop from the store with a matching id
    // Updates [information display] and [feature list] with information about it
    // Does not change the visibility of either component/element
    async updateLaptopInfo(laptopId) {
        let laptop = null;
        try {
            this.currentLaptopRequestId++;
            this.showLaptopLoadingSpinner();
            laptop = await Store.getLaptopById(laptopId);
        }
        catch(e) {
            this.displayMessage(`We are terribly, terribly sorry. The laptop you requested could not be found in The Store.`);
            this.hideLaptopInfo();
            this.hideLaptopLoadingSpinner(true);
            return;
        }
        finally {
            this.latestLaptopRequestId++;
        }

        // New requests have been queued and are still pending; wait for and resolve only the latest.
        if (this.currentLaptopRequestId !== this.latestLaptopRequestId) return;
        
        // hideLaptopLoadingSpinner is just a timeout, I'm not sure if that can even fail.
        try {
            await this.hideLaptopLoadingSpinner();
        }
        catch(e) {
            this.hideLaptopLoadingSpinner(true);
        }
        // Update feature list
        const featureListHTML = laptop.features.map(feature => `<li>${feature}</li>`).join('')
        this.elStoreFeatureListDisplay.innerHTML = featureListHTML;

        // Update information display
        const imageSrc = laptop.imageUrl;
        const imageAlt = 'A view of the marvelous ' + laptop.name;
        const lapTopInfoHTML = `
            <img src="${imageSrc}" alt="${imageAlt}" />
            <div>
                <h2>${laptop.name}</h2>
                <p>${laptop.description}</p>
            </div>
            <div>
                <h2>${laptop.price}</h2>
                <a class="button" />Buy now</a>
            </div>
        `;

        // Hook up the new buy button to the purchase function.
        this.elLaptopInfoDisplay.innerHTML = lapTopInfoHTML;
        let elBuyLaptopBtn = document.querySelector('#laptopContainer a.button');
        elBuyLaptopBtn.addEventListener('click', this.purchaseLaptop.bind(this, laptop));
    }

    // Contacts the store and attempts to purchase the supplied laptop.
    async purchaseLaptop(laptop) {
        try {
            await Store.purchaseLaptop(laptop, this.user);
        } catch(e) {
            // Codes are just used here because that might be reasonable to expect from an API. 
            if (e.code === -1) {
                this.displayMessage("You don't have sufficient funds to purchase this marvelous piece of technology. Work harder, try later.");
            }
            else if (e.code === -2) {
                this.displayMessage("The laptop you requested doesn't exist.");
            }
            else {
                this.displayMessage('Unknown error occured during laptop purchase.');
            }
            return;
        }
        
        // Purchase successful. Money lost, laptop gained. Update user state & display. Show annoying congratulatory alert.
        this.user.ownedLaptops.push(laptop);
        this.displayBalances();
        this.displayOwnedLaptops();
        this.displayMessage(`Congratulations on your purchase of the ${laptop.name}. We hope to see you back very, <i>very soon</i>.`, this.MESSAGE_TYPE.FRIENDLY);
    }

    displayOwnedLaptops() {
        const laptopNames = this.user.ownedLaptops.map(laptop => laptop.name);

        // If user owns several of the same laptop, we want to display that as: '5x HP Crashalot' - not list them all.
        const uniqueLaptops = Array.from(new Set(laptopNames))
        const uniqueLaptopsAndCount = uniqueLaptops
                                        .map(e => [e, laptopNames.filter(v => v === e).length])
                                        .map(e => `${e[1]}x ${e[0]}`);
        this.elOwnedLaptopsDisplay.innerHTML = uniqueLaptopsAndCount.map(laptopAndCount => '<li>' + laptopAndCount + '</li>').join('');
    }

    // Display current bank balance and loan - the latter only if non-zero.
    displayBalances() {
        const currentLoan = this.user.bankAccount.getCurrentLoan();
        const currentLoanHTML = currentLoan === 0 ? '' : `<p>Loan: </p><p>${currentLoan} kr</p>`;
        this.elBankBalanceDisplay.innerHTML = `${this.user.bankAccount.getCurrentBalance()} kr`;
        this.elBankLoanDisplay.innerHTML = currentLoanHTML;
        this.elWorkPayDisplay.innerHTML = `${this.user.workAccount.getCurrentBalance()} kr`;
    }

    // If a non-zero loan exists, displays a button that allows repaying it.
    // If no loan exists, this button is hidden instead.
    displayRepayLoanButton() {
        if (this.user.bankAccount.getCurrentLoan() > 0) {
            this.elRepayLoanBtn.style.display = 'inline-block';
        }
        else {
            this.elRepayLoanBtn.style.display = 'none';
        }
    }

    // Repay loan and display updated information
    repayLoan() {
        this.user.workAccount.repayLoan(this.user.bankAccount);
        this.displayRepayLoanButton();
        this.displayBalances();
    }

    // Work and display updated information
    work() {
        this.user.workAccount.work();
        this.displayBalances();
    }

    // Transfer money from work balance to bank balance, and display updated information
    transferWorkBalanceToBank() {
        this.user.workAccount.transferToBankBalance(this.user.bankAccount);
        this.displayBalances();
    }

    // Prompts the user for an amount to loan, then very kindly asks the bank for that loan
    takeLoan(moneyToLoan) {
        if (isNaN(moneyToLoan)) {
            this.displayMessage('Please enter an amount to loan.');
            return;
        }
        
        try {
            this.user.bankAccount.takeLoan(moneyToLoan, this.user.ownedLaptops.length >= 1);
        }
        catch(e) {
            this.displayMessage(e.message);
        }
        
        // Display updated info
        this.displayBalances();
        this.displayRepayLoanButton();
    }
}

const komputerStore = new KomputerStore();
