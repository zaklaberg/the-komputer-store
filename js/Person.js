import BankAccount from './BankAccount.js'
import WorkAccount from './WorkAccount.js'

class Person {
    constructor(name) {
        this.bankAccount = BankAccount();
        this.workAccount = WorkAccount();
        this.name = name;
        this.ownedLaptops = [];
        window.ba = this.bankAccount;
    }
}

export default Person;