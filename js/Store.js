/*
Store module. 
Can be queried for available laptops.
Allows purchase of available laptops.
*/
let cachedLaptops = null;
const STORE_URL = 'http://localhost:3000/laptops';

// Purchase a laptop. The 'laptop' argument is expected to be of class Laptop(see js/Laptop.js)
// The user must have a bankAccount attribute, which is expected to be created by the factory in js/BankAccount.js
// The function is async because it also verifies whether the laptop actually exists.
async function purchaseLaptop(laptop, user) {
    // Price is given in text, e.g. '500 kr'. Extract the number.
    const priceAsNumber = parseInt(laptop.price.replace(/[kr]|\s/g, ''), 10);
    const laptops = await fetchLaptopsFromServer();

    // Fail if user doesn't have enough money in bank
    if (priceAsNumber > user.bankAccount.getCurrentBalance()) {
        const err = new Error('Insufficient funds');
        err.code = -1;
        throw err;
    }
    // Fail if the specified laptop isn't among the currently available products
    else if (laptops.find(lp => lp.name === laptop.name) === undefined) {
        const err = new Error('No laptop by that name is available.');
        err.code = -2;
        throw err;
    }

    // Everything is fine. Return the laptop, and take the money it costs.
    user.bankAccount.removeFromBalance(priceAsNumber);
    return laptop;
}

function getLaptopByProperty(property, propertyValue, forceFetch = false) {
    let laptops = null;
    const throwIfNoLaptopFound = laptop => {
        if (laptop === undefined) throw new Error(`Laptop with ${property} ${propertyValue} could not be found in The Store.`);
        return laptop;
    };

    const waitForImageToLoad = laptop => new Promise(resolve => {
        const img = new Image();
        img.src = laptop.imageUrl;
        img.onload = () => resolve(laptop);
    });

    laptops = fetchLaptopsFromServer(forceFetch);

    return laptops.then(laptops => laptops.find(el => el[property] === propertyValue))
                .then(throwIfNoLaptopFound)
                .then(waitForImageToLoad)
}

function getLaptopById(id, forceFetch = false) {
    return getLaptopByProperty('id', id, forceFetch);
}

function getLaptopByName(name, forceFetch = false) {
    return getLaptopByProperty('name', name, forceFetch);
}

function fetchLaptopsFromServer(forceFetch = false) {
    const handleBadResponse = resp => {
        if (!resp.ok) throw new Error(`Bad response: ${resp.statusText}`)
        return resp;
    }
    const getJSONFromResponse = resp => resp.json();

    const cacheLaptops = laptops => { cachedLaptops = laptops; return laptops; }

    if (cachedLaptops !== null && !forceFetch) {
        return Promise.resolve(cachedLaptops);
    }
    
    return fetch(STORE_URL)
        .then(handleBadResponse)
        .then(getJSONFromResponse)
        .then(cacheLaptops)
        .catch(e => {
            throw new Error(`Could not retrieve available laptops. Reason: ${e.message}`);
        });
}

function getAvailableLaptops(forceFetch = false) {
    return fetchLaptopsFromServer(forceFetch);
}

export default {
    purchaseLaptop,
    getLaptopByName,
    getLaptopById,
    getAvailableLaptops
}