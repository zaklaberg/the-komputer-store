/*
Class to display a prompt.
Constructor expects a query string which can be used to grab the parent node. 
The prompt will appear in the middle of its parent area.
Call .dispose() to remove the prompt from the DOM.

TODO: Allow a custom form, return form data on action click. But - we won't need that right now.
*/
class MoneyPrompt {
    constructor(parentQuery, title, actionString, onAction = () => {}, onCancel = () => {}) {
        const promptId = 'exceptionalCustomPrompt';
        const elemHTML = `<div id="${promptId}">
                            <span>
                                <h2>${title}</h2>
                                <div>
                                    <input type="number" placeholder="420" /><span>&nbsp;kr</span>
                                </div>
                                <div>
                                    <a class="button">${actionString}</a>
                                    <a class="button">Cancel</a>
                                </div>
                            </span>
                        </div>`;

        // insertAdjacentHTML will happily insert an item with an id that already exists. So let's check for it
        const ourIdExists = document.getElementById(promptId) !== null;
        if (ourIdExists) {
            throw new Error(`Id used by prompt(${promptId}) already exists.`);
        }

        // All good, let's add it.
        document.querySelector(parentQuery).insertAdjacentHTML('beforeend', elemHTML);

        // Store own HTML element + form element for show/hide/action submit
        this.elNumberInput = document.querySelector(`#${promptId} input`);
        this.elPrompt = document.getElementById(promptId);
        this.elHTML = document.getElementsByTagName('html')[0];

        // And let's listen for button clicks
        const buttons = document.querySelectorAll(`#${promptId} a.button`);
        this.elActionBtn = buttons[0];
        this.elCancelBtn = buttons[1];

        this.elActionBtn.addEventListener('click', () => {
            onAction(this.elNumberInput.valueAsNumber);
            this.elNumberInput.value = '';
            this.hide();
        });
        this.elCancelBtn.addEventListener('click', () => {
            onCancel();
            this.elNumberInput.value = '';
            this.hide();
        });
    }

    dispose() {
        this.elPrompt.remove(); // Also removes listeners - provided no one keeps a reference!
        this.elPrompt = null;
    }

    show() {
        // We need to extend the height of the prompt background to the entire scrollable area
        // to prevent interaction with the background. Only relevant for small size screens.
        const fullScrollHeight = this.elHTML.scrollHeight;
        this.elPrompt.style.display = 'flex';
        this.elPrompt.style.height = `${fullScrollHeight}px`;
        this.elNumberInput.focus();
    }

    hide() {
        this.elPrompt.style.display = 'none';
    }
}

export default MoneyPrompt;